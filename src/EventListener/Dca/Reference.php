<?php

namespace Mindbird\Contao\Reference\EventListener\Dca;

use Contao\Backend;
use Contao\DataContainer;
use Mindbird\Contao\Reference\Model\ReferenceArchiveModel;
use Mindbird\Contao\Reference\Model\ReferenceCategoryModel;

class Reference extends Backend
{
    public function getReferenceTemplates()
    {
        return static::getTemplateGroup('reference_');
    }

    public function getReferenceFilterTemplates()
    {
        return static::getTemplateGroup('reference_filter_');
    }

    public function listReference($row)
    {
        return '<div>' . $row['title'] . '</div>';
    }

    public function onloadCallback(DataContainer $dc)
    {
        $objreferenceArchive = ReferenceArchiveModel::findByPk($dc->id);

        switch ($objreferenceArchive->sort_order) {
            case 2:
                $GLOBALS['TL_DCA']['tl_reference']['list']['sorting']['mode'] = 4;
                $GLOBALS['TL_DCA']['tl_reference']['list']['sorting']['fields'] = array(
                    'sorting'
                );
                $GLOBALS['TL_DCA']['tl_reference']['list']['sorting']['headerFields'] = array(
                    'title'
                );
                break;
            case 1:
            default:

                // Nothing to do
                break;
        }
    }

    public function optionsCallbackCategory($dc): array
    {
        $categories = ReferenceCategoryModel::findBy ( 'pid', $dc->activeRecord->pid, array (
            'order' => 'title ASC'
        ) );
        $category = array();
        if ($categories) {
            while ($categories->next()) {
                $category[$categories->id] = $categories->title;
            }
        }

        return $category;
    }

    public function optionsCallbackReferenceCategory($dc): array
    {
        $categories = ReferenceCategoryModel::findBy ( 'pid', $dc->activeRecord->reference_archiv, array (
            'order' => 'title ASC'
        ) );
        $category = array();
        if ($categories) {
            while ($categories->next()) {
                $category[$categories->id] = $categories->title;
            }
        }

        return $category;
    }

    /**
     * Set the timestamp to 00:00:00 (see #26)
     *
     * @param integer $value
     *
     * @return integer
     */
    public function loadDate($value)
    {
        if($value== 0) $value= time();
        return strtotime(date('Y-m-d', $value) . ' 00:00:00');
    }
}
