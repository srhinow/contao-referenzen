<?php

/** Backend */
$GLOBALS['BE_MOD']['content']['reference'] = array(
    'tables' => array(
        'tl_reference_archive',
        'tl_reference',
        'tl_reference_category',
        'tl_content'
    )
);

/** Frontend */
array_insert($GLOBALS ['FE_MOD'] ['reference'], 1, array(
    'reference_list' => 'Mindbird\Contao\Reference\Module\ReferenceList',
    'reference_detail' => 'Mindbird\Contao\Reference\Module\ReferenceDetail'
));

/** Hooks */
$GLOBALS ['TL_HOOKS'] ['getSearchablePages'] [] = array(
    'Mindbird\Contao\Reference\EventListener\Hook\ReferenceBackend',
    'getSearchablePages'
);

/** Models */
$GLOBALS['TL_MODELS']['tl_reference'] = 'Mindbird\Contao\Reference\Model\ReferenceModel';
$GLOBALS['TL_MODELS']['tl_reference_archive'] = 'Mindbird\Contao\Reference\Model\ReferenceArchiveModel';
$GLOBALS['TL_MODELS']['tl_reference_category'] = 'Mindbird\Contao\Reference\Model\ReferenceCategoryModel';
