<?php

/**
 * Table tl_reference_category
 */
$GLOBALS ['TL_DCA'] ['tl_reference_category'] = [
    // Config
    'config' => [
            'dataContainer' => 'Table',
            'enableVersioning' => true,
            'switchToEdit' => true,
            'ptable' => 'tl_reference_archive',
            'sql' => [
                    'keys' => [
                            'id' => 'primary',
                            'pid' => 'index'
                    ]
            ]
    ],

    // List
    'list' => [
            'sorting' => [
                    'mode' => 1,
                    'fields' => [
                            'title'
                    ],
                    'flag' => 1,
                    'panelLayout' => 'filter;search,limit'
            ],
            'label' => [
                    'fields' => [
                            'title'
                    ],
                    'format' => '%s'
            ],
            'global_operations' => [
                    'all' => [
                            'label' => &$GLOBALS ['TL_LANG'] ['MSC'] ['all'],
                            'href' => 'act=select',
                            'class' => 'header_edit_all',
                            'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"'
                    ]
            ],
            'operations' => [

                    'edit' => [
                            'label' => &$GLOBALS ['TL_LANG'] ['tl_reference_category'] ['edit'],
                            'href' => 'act=edit',
                            'icon' => 'header.gif'
                    ],
                    'copy' => [
                            'label' => &$GLOBALS ['TL_LANG'] ['tl_reference_category'] ['copy'],
                            'href' => 'act=copy',
                            'icon' => 'copy.gif'
                    ],
                    'delete' => [
                            'label' => &$GLOBALS ['TL_LANG'] ['tl_reference_category'] ['delete'],
                            'href' => 'act=delete',
                            'icon' => 'delete.gif',
                            'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS ['TL_LANG'] ['MSC'] ['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
                    ]
            ]
    ],

    // Palettes
    'palettes' => [
            'default' => '{title_legend},title;{filter_legend},filterPage;{extend_legend},show'
    ],
    // Fields
    'fields' => [
        'id' => [
                'sql' => "int(10) unsigned NOT NULL auto_increment"
        ],
        'pid' => [
                'sql' => "int(10) unsigned NOT NULL default '0'"
        ],
        'tstamp' => [
                'sql' => "int(10) unsigned NOT NULL default '0'"
        ],
        'title' => [
                'label' => &$GLOBALS ['TL_LANG'] ['tl_reference_category'] ['title'],
                'exclude' => true,
                'search' => true,
                'inputType' => 'text',
                'eval' => [
                        'mandatory' => true,
                        'maxlength' => 255
                ],
                'sql' => "varchar(255) NOT NULL default ''"
        ],
        'filterPage' =>
            [
            'label'                   => &$GLOBALS['TL_LANG']['tl_reference_category']['filterPage'],
            'exclude'                 => true,
            'inputType'               => 'pageTree',
            'foreignKey'              => 'tl_page.title',
            'eval'                    => ['mandatory'=>false, 'fieldType'=>'radio', 'tl_class'=>'clr'],
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
            'relation'                => ['type'=>'hasOne', 'load'=>'eager']
            ],
        'show' =>
            [
            'label'                   => &$GLOBALS['TL_LANG']['tl_reference_category']['show'],
            'exclude'                 => true,
            'filter'                  => true,
            'inputType'               => 'checkbox',
            'sql'                     => "char(1) NOT NULL default ''"
            ]
    ]
];
