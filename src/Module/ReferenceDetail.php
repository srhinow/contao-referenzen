<?php

namespace Mindbird\Contao\Reference\Module;

use Contao\BackendTemplate;
use Contao\ContentModel;
use Contao\Controller;
use Contao\Database;
use Contao\FilesModel;
use Contao\Input;
use Contao\Module;
use Mindbird\Contao\Reference\Model\ReferenceModel;

class ReferenceDetail extends Module
{
	
	/**
	 * Template
	 *
	 * @var string
	 */
	protected $strTemplate = 'mod_reference_detail';
	
	public function generate()
    {
		if (TL_MODE === 'BE') {
			$template = new BackendTemplate ( 'be_wildcard' );
				
			$template->wildcard = '### REFERENZEN DETAILS ###';
			$template->title = $this->headline;
			$template->id = $this->id;
			$template->link = $this->name;
			$template->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;
				
			return $template->parse ();
		}

		return parent::generate ();
	}
	
	protected function compile()
    {
        $db = Database::getInstance();

        if(null === ($referenceId = Input::get('auto_item'))) {
            return '';
        }

        if (null === ($objReference = ReferenceModel::findByPk ( $referenceId ))) {
            return '';
        }

        global $objPage;
        $objPage->pageTitle = $objReference->title;

        $image = FilesModel::findByPk ( $objReference->image );
        if ($image) {
            Controller::addImageToTemplate($this->Template, array(
                'singleSRC' => $image->path,
                'size' => deserialize ( $this->imgSize ),
                'alt' => $objReference->title
            ));
        }

        // Get Categories
        $categories = deserialize ( $objReference->category );
        $categoryArr = [];
        if (\count( $categories ) > 0) {
            $referenceCategories = $db->prepare ( "SELECT * FROM tl_reference_category WHERE id IN(" . implode ( ',', $categories ) . ")" )->execute (  );
            while ( $referenceCategories->next () ) {
                $categoryArr[$referenceCategories->id] = $referenceCategories->title;
            }
        }

        $content = '';
        $contentElement = ContentModel::findPublishedByPidAndTable($referenceId, 'tl_reference');
        if ($contentElement !== null)
        {
            while ($contentElement->next())
            {
                $content .= static::getContentElement($contentElement->current());
            }
        }

        $this->Template->content = $content;
        $this->Template->reference = $reference;
        $this->Template->categories = $categoryArr;
    }
}
